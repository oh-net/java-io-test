package utils;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class DisplayUtils {
    public static void printlnError(String msg) {
        System.err.println("жжж [" + LocalDateTime.now().format(DateTimeFormatter.ofPattern("Y-M-d H:m:s")) + "] " + msg);
    }

    public static void printlnMini(String msg) {
        System.out.print("■" + msg);
    }

    public static void printlnNormal(String msg) {
        System.out.println("иии [" + LocalDateTime.now().format(DateTimeFormatter.ofPattern("Y-M-d H:m:s")) + "] " + msg);
    }

}
