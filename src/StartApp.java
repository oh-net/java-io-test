import utils.DisplayUtils;

import javax.xml.bind.SchemaOutputResolver;
import java.io.*;

public class StartApp {
    private static final String FILE_PATH = "/home/regchen/mycode/java-io-test/data_dir/hallo.txt";

    public static void main(String[] args) {
        File file = new File(FILE_PATH);
        if (!file.exists()){
            try{
                // 创建文件
                boolean flagForCreateNew = file.createNewFile();
                if (!flagForCreateNew){
                    DisplayUtils.printlnError("创建文件失败");
                } else {
                    //
                    DisplayUtils.printlnNormal("文件已创建");
                }
            }catch (IOException e){
                DisplayUtils.printlnError("创建文件异常： " +e.getMessage());
            }
        } else {
            DisplayUtils.printlnNormal("文件已存在：" + FILE_PATH);
            // 显示文件分隔符
            DisplayUtils.printlnNormal("文件分隔符 Separator: " + File.separator);
            DisplayUtils.printlnNormal("文件分隔符 PathSeparator: " + File.pathSeparator);
        }

        // 删除文件
//        boolean flagForDeleteFile = file.delete();
//        if (flagForDeleteFile){
//            DisplayUtils.printlnNormal("文件已删除");
//        } else {
//            DisplayUtils.printlnNormal("文件删除失败");
//        }

        // 显示目录下文件，包括隐藏
        File dir = new File("/home/regchen" + File.separator);
        File[] fileList = dir.listFiles();
        if (fileList != null) {
            for (File f : fileList) {
                DisplayUtils.printlnMini(f + "\n");
            }
        } else {
            DisplayUtils.printlnNormal("指定的句柄不是目录或目录下没有文件");
        }
    }
}
